import axios from 'axios';

class GetDataApi {
    async getData(url) {
        try {
            const btn = document.querySelector('.js-button');
            btn.disabled = true;
            const response = await axios.get(url, {});
            return response.data.message;
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }
}

export const getDataApi = new GetDataApi();