import '../style/main.less';
import 'regenerator-runtime/runtime';
import { getDataApi } from '../js/utils/getDataApi.js'

const API_URL = 'https://dog.ceo/api/breed/shiba/images/random';
const btn = document.querySelector('.js-button');
const image = document.querySelector('.js-img');

const historyCount = document.querySelector('.js-count');
let countImage = 1;


btn.addEventListener('click', async () => {
    const data = await getDataApi.getData(API_URL)
    updateImage(data)
    addHistory()
    disableButton()
})

function updateImage(url) {
    image.src = url
}

function addHistory() {
    countImage++
    historyCount.textContent = countImage;
}

function disableButton() {
    btn.disabled = false;
}